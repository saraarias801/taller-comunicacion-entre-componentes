import { Injectable } from '@angular/core';
import {Categoria} from '../app/categoria'

@Injectable({
  providedIn: 'root'
})
export class ImagenesService {
  
  public categorias:Categoria[]
  constructor() {
    this.categorias = [
      {
        nombre: "Chicas",
        imagenes: ["https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220548729.jpg/EG11/resize/690x-1/quality/75/format/jpg",
                   "https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220484118.jpg/EG11/resize/690x-1/quality/75/format/jpg",
                   "https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220541043.jpg/EG11/resize/690x-1/quality/75/format/jpg",
                   "https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220536645.jpg/EG11/resize/690x-1/quality/75/format/jpg"]
      },
      {
        nombre: "Chicos",
        imagenes: ["https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-14922047826.jpg/EG11/resize/690x-1/quality/75/format/jpg",
                   "https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220542606.jpg/EG11/resize/690x-1/quality/75/format/jpg",
                   "https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220549901.jpg/EG11/resize/690x-1/quality/75/format/jpg",
                   "https://images.eurogamer.net/2017/articles/1/8/9/5/4/7/8/persona-5-confidants-respuestas-a-todas-las-conversaciones-de-tus-social-links-para-subirlos-al-maximo-149220482762.jpg/EG11/resize/690x-1/quality/75/format/jpg"]
      }
    ]
   }
}
